var globalCard;

document.addEventListener('DOMContentLoaded', function (e) {

	chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
		var tab = tabs[0];
		chrome.tabs.sendMessage(tab.id, {task: 'generateCard'}, function (card) {
			globalCard = card;
			document.getElementById('card_front').textContent = card.front;
			document.getElementById('card_back').textContent = card.back;
		});
	});

	trelloAuth();
});

function trelloAuth() {
	var key;
	var trelloApiTag;
	chrome.storage.sync.get({trello_key: ''}, function (items) {
		if (items.trello_key) {
			key = items.trello_key;
			trelloApiTag = document.createElement('script');
			trelloApiTag.src = "https://api.trello.com/1/client.js?key=" + key;
			trelloApiTag.addEventListener('load', function (e) {
				Trello.authorize({
					interactive: false,
					success: setTrelloActive,
					error: function () {
						Trello.authorize({
							type: "redirect",
							name: "Active Collab→Trello Flow",
							perist: true,
							scope: {read: true, write: true, account: false},
							expiration: "never",
							success: setTrelloActive
						});
					}
				});
			});
			document.head.appendChild(trelloApiTag);
		}
	});
}

function setTrelloActive() {
	var form = document.getElementById('trello_form');
	var boardSelect = document.getElementById('board');
	var listField = document.getElementById('list-field');
	var listSelect = document.getElementById('list');
	var boardParams = {
		filter: 'open',
		lists: 'open'
	};

	Trello.get('members/me/boards', boardParams, function (boards) {
		var boardNames = {};
		var listNames = {};

		boards.forEach(function (board) {
			boardNames[board.id] = board.name;
			listNames[board.id] = {};
			board.lists.forEach(function (list) {
				listNames[board.id][list.id] = list.name;
			});
		});

		fillSelectList(boardSelect, boardNames);

		boardSelect.disabled = false;
		boardSelect.addEventListener('change', function () {
			if (boardSelect.value) {
				fillSelectList(listSelect, listNames[boardSelect.value]);
				listSelect.disabled = false;
			}
		});

		form.addEventListener('submit', function (e) {
			var board = boardSelect.value;
			var list = listSelect.value;
			var position = document.getElementById('position').value;
			var card = {
				front: document.getElementById('card_front').textContent,
				back: document.getElementById('card_back').textContent
			};
			e.preventDefault();

			if (board && list && listNames[board].hasOwnProperty(list)) {
				createCard(globalCard, list, position);
			}
		});
	});
}

function fillSelectList(select, options) {
	var container = document.createDocumentFragment();
	var i;
	var opt;
	for (i in options) if (options.hasOwnProperty(i)) {
		opt = document.createElement('option');
		opt.value = i;
		opt.textContent = options[i];
		container.appendChild(opt);
	}
	select.innerHTML = '<option value="">- Select -</option>';
	select.appendChild(container);
}

function createCard(card, list, position) {
	var cardParams = {
		name: card.front,
		desc: card.back,
		idList: list,
		pos: position
	};

	if (card.meta.due_on) {
		cardParams.due = card.meta.due_on;
	}

	Trello.post('cards', cardParams, function (newCard) {
		chrome.tabs.create({url: newCard.url});
	});
}
