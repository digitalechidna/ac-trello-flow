chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.task == 'addIcon') {
		chrome.pageAction.show(sender.tab.id);
		sendResponse({pageActionVisible: true});

		return true;
	}
});
