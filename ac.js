(function (undefined) {
	var taskOption = document.getElementById('object_quick_option_new_task');
	var trelloOption = document.createElement('li');
	var trelloLink = document.createElement('a');
	var trelloText = document.createElement('span');

	var getMeta = function (properties) {
		var key;
		var i;
		var el;
		var content;
		var meta = {};
		for (i = 0; i < properties.length; i += 1) {
			el = properties[i];
			key = el.textContent.toLowerCase().replace(/[^a-z]/, '_');
			content = el.nextSibling;

			// Make sure content isn't just whitespace.
			while (content.nodeType == Text.TEXT_NODE) {
				content = content.nextSibling;
			}

			meta[key] = content.textContent.trim();
		}
		return meta;
	};

	var ticketExtractor = function () {
		var titleData;
		var body;
		var properties;
		var client;
		var project;
		var title;
		var ticketNumber;
		var url;
		var card = {front: '', back: ''};

		titleData = document.getElementById('page_title').textContent.split('|');
		body = document.querySelectorAll('#page_content .body.content')[0].textContent;
		properties = document.querySelectorAll('#page_content dl.properties dt');

		title = titleData.pop().trim().split(':');
		project = titleData.pop().trim();
		client = titleData.pop().trim();
		ticketNumber = '{' + client + '} ' + title.shift().replace('#', '').trim();
		card.front = ticketNumber + ': ' + title.join(':').trim();

		url = window.location;
		card.back = '[original ticket](' + url + ')\n\n' + body;

		card.meta = getMeta(properties);

		return card;
	};

	chrome.runtime.sendMessage({task: 'addIcon'});
	chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
		if (request.task == 'generateCard') {
			sendResponse(ticketExtractor());
		}
		return true;
	});
}());
