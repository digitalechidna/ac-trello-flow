var acUrl;
var trelloKey;
var trelloApiTag = document.createElement('script');
var acTimer;

function activateTrelloApi(key) {
	trelloApiTag.src="https://api.trello.com/1/client.js?key=" + key;
	trelloApiTag.addEventListener('load', function (e) {
		Trello.authorize({
			interactive: false,
			success: setTrelloActive,
			error: function () {
				Trello.authorize({
						type: "redirect",
					name: "Active Collab→Trello Flow",
					perist: true,
					scope: {read: true, write: true, account: false},
					expiration: "never",
					success: setTrelloActive,
				});
			}
		});
	});
	document.head.appendChild(trelloApiTag);
}

function setTrelloActive() {
	var trelloStatus = document.getElementById('trello_status');
	if (Trello.authorized()) {
		trelloStatus.className = "status success";
		trelloStatus.innerHTML = "Authorized";
	}
}

function debounceAC(e) {
  clearTimeout(acTimer);
  acTimer = setTimeout(saveAC, 100);
}

function saveAC() {
	var url = document.getElementById('ac_url').value;
	setAcStatus('Saving', '');
	chrome.storage.sync.set({ac_url: url}, function () {
		setAcStatus('Saved', 'success');
	});
}

function setAcStatus(label, state) {
	var acStatus = document.getElementById('ac_status');
	acStatus.className = "status " + state;
	acStatus.innerHTML = label;
}

function restoreSettings() {
	chrome.storage.sync.get({ac_url: '', trello_key: ''}, function (items) {
		if (items.ac_url) {
			document.getElementById('ac_url').value = items.ac_url;
			setAcStatus('Saved', 'success');
		}
		if (items.trello_key) {
			document.getElementById('trello_key').value = items.trello_key;
			activateTrelloApi(items.trello_key);
		}
	});
}

function trelloAuth() {
	var key = document.getElementById('trello_key').value;
	chrome.storage.sync.set({trello_key: key}, function () {
		activateTrelloApi(key);
	});
}

document.getElementById('ac_url').addEventListener('change', debounceAC);
document.getElementById('ac_url').addEventListener('keyup', debounceAC);
document.getElementById('trello_auth').addEventListener('click', trelloAuth);
restoreSettings();
